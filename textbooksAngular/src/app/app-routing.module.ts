import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule,} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PrinterFormComponent } from './printer-form/printer-form.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'printerForm', component: PrinterFormComponent },
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],

})
export class AppRoutingModule { }