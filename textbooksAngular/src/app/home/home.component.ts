import { Component, OnInit,ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  successMsg: boolean;
  msg: string;
  @ViewChild('printerForm') printerForm: NgForm;
  constructor() { }

   ngOnInit(): void {
    this.successMsg = false;
    setInterval(() => {
      this.successMsg = false;
    }, 8000);
    this.printerForm.controls.printerDistrict.setValue("0");
    console.log(this.printerForm.controls.printerDistrict);
  }

  showModal: boolean;
  content: string;
  title: string;
  //Bootstrap Modal Open event
  show() {
    this.showModal = true; // Show-Hide Modal Check
    this.content = "This is content!!"; // Dynamic Data
    this.title = "This is title!!";    // Dynamic Data
  }
  //Bootstrap Modal Close event
  hide() {
    this.showModal = false;
  }

  onClickSubmit(data) {
    alert("With Stringify :" + data);
    alert("Entered printerName : " + data.printerName);
    alert("Entered printerDistrict : " + data.printerDistrict);
    alert("Entered PrinterAddress : " + data.printerAddress);
    alert("Entered Email id : " + data.printerEmail);
    this.showModal = false;
    this.msg = "Printer data updated successfully";
    this.successMsg = true;
  }

  deleteRow() {
    this.successMsg = true;
    this.msg = "Printer Deleted successfully";
  }

}