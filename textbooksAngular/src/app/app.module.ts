import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PrinterFormComponent } from './printer-form/printer-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PrinterFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class PrinterForm {
  printerName: string;
  printerDistrict: string;
  printerAddress: string;
  printerEmail: string;
  
  
  // public set PrinterDistrict(v : string) {
  //   this.printerDistrict = v;
  // }
  
  // public get PrinterDistrict() {
  //   return this.printerDistrict;
  // }


}


